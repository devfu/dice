# Super-simple dice roller. Get your nerd on.
class Die

  # set up a die
  def initialize sides=20
    @sides = sides.to_i
  end

  # roll a given number of these dice
  def roll number = 1
    rolls = []
    number.to_i.times { rolls << (rand(@sides) + 1).to_i }
    rolls
  end

  module Roller

    def const_missing name
      if name.to_s =~ /^D(\d+)$/
        Die.new($1).roll
      elsif name.to_s =~ /^D(\d+)x(\d+)$/
        Die.new($1).roll($2)
      else
        super
      end
    end

  end

end

Object.send :extend, Die::Roller
