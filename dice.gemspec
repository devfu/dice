Gem::Specification.new do |s|
  s.name = 'dice'
  s.version = '0.0.3'
  s.summary = 'Dice are delicious!'
  s.files = Dir['lib/**/*.rb'] + ['README.rdoc']
  s.executables = ['roll']
  s.extra_rdoc_files = ['README.rdoc']
end
