Dice
====

Install
-------

    $ sudo gem install dice

Command-line usage
------------------

    $ roll D20
      [18]
    $ roll D20x3
      [7, 8, 3]
    $ roll D2x3 D4 D09 D10x4
      [2, 1, 2]
      [2]
      [9]
      [3, 9, 8, 8]

Ruby usage
----------

    $ irb
    >> require 'dice'
    => true
    >> D20
    => [12]
    >> D20x3
    => [9, 13, 6]
